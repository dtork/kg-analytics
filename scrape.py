import tweepy
import csv
import json

# Load Twitter API credentials

with open('creds.json') as cred_data:
    info = json.load(cred_data)
    consumer_key = info['CONSUMER_KEY']
    consumer_secret = info['CONSUMER_SECRET']
    access_key = info['ACCESS_TOKEN']
    access_secret = info['ACCESS_SECRET']
    target = info['TARGET']


def get_all_tweets(screen_name):
    """Get 3240 tweets from user."""
    # Auth and initialization
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)

    # Initialization of a list to hold all Tweets
    all_tweets = []

    # Get with multiple requests of 200 tweets each
    new_tweets = api.user_timeline(screen_name=screen_name, count=200)

    # Save most recent tweets
    all_tweets.extend(new_tweets)

    # Save ID of 1 less than the oldest tweet
    oldest_tweet = all_tweets[-1].id - 1

    # Continue until none are left
    while len(new_tweets) > 0:
        # The max_id param will be used subsequently to prevent duplicates
        new_tweets = api.user_timeline(screen_name=screen_name, count=200, max_id=oldest_tweet)

        # Save most recent tweets
        all_tweets.extend(new_tweets)

        # ID is updated to oldest tweet - 1 to keep track
        oldest_tweet = all_tweets[-1].id - 1
        print(f'...{len(all_tweets)} tweets have been downloaded so far')

    # Transform tweets into a 2D array that will be used to populate the CSV
    #outtweets = [[tweet.id_str, tweet.created_at, tweet.text.encode('utf-8')] for tweet in all_tweets]
    outtweets = [[tweet.id_str, tweet.created_at, tweet.text] for tweet in all_tweets]

    # Writing CSV file
    with open(f'{screen_name}_tweets.csv', 'w', encoding='utf8') as f:
        writer = csv.writer(f)
        writer.writerow(['id', 'created_at', 'text'])
        writer.writerows(outtweets)


if __name__ == '__main__':
    get_all_tweets(target)