
## Resources
* [Tweepy - Twitter for Python](https://www.pythoncentral.io/introduction-to-tweepy-twitter-for-python/)
* [A beginner's guide to collecting twitter data](https://knightlab.northwestern.edu/2014/03/15/a-beginners-guide-to-collecting-twitter-data-and-a-bit-of-web-scraping/)
* [`python-twitter`](https://python-twitter.readthedocs.io/en/latest/getting_started.html)
* [Generating WordClouds in Python](https://www.datacamp.com/community/tutorials/wordcloud-python)
    * [`wordcloud`](https://github.com/amueller/word_cloud)

## TODO
* [ ] Use Tesseract OCR to read PDFs of board meeting minutes
    * [Using Tesseract OCR with PDF scans](http://kiirani.com/2013/03/22/tesseract-pdf.html)
    * [Using Tesseract OCR with Python](https://www.pyimagesearch.com/2017/07/10/using-tesseract-ocr-python/)
    * [Tesseract on Linux](https://linuxhint.com/install-tesseract-ocr-linux/) (for running on RPI)
    * [ ] Find a way to auto-rotate PDF pages that are landscape orientation
    * [Using `pdftabextract` to liberate tabular data](https://datascience.blog.wzb.eu/2017/02/16/data-mining-ocr-pdfs-using-pdftabextract-to-liberate-tabular-data-from-scanned-documents/)
