"""Extract jpg's from pdf's. Quick and dirty.

https://pymupdf.readthedocs.io/en/latest/
"""
import sys
import fitz


def convert(pdf_path):
    """Convert a PDF to multiple image files."""
    doc = fitz.open(pdf_path)
    for i in range(len(doc)):
        for img in doc.getPageImageList(i):
            xref = img[0]  # check if this xref was handled already?
            pix = fitz.Pixmap(doc, xref)
            if pix.n < 5:  # this is GRAY or RGB
                pix.writePNG(f'pdfconverts2/p{i}-{xref}.png')
            else:  # CMYK needs to be converted to RGB first
                pix1 = fitz.Pixmap(fitz.csRGB, pix)  # make RGB pixmap copy
                pix1.writePNG(f'pdfconverts2/p{i}-{xref}.png')
                pix1 = None  # release storage early (optional)
            pix = None  # release storage early (optional)


if __name__ == '__main__':
    PDF_FILE_PATH = sys.argv[1]
    convert(PDF_FILE_PATH)
